#Common functions to be used across all the tasks#

from scipy import misc
from numpy import ndenumerate
import matplotlib.pyplot as plt
import math


#This function load the image array from a file
def readimage(imageName):
    return misc.imread(imageName)


#This function preview the image using the 'matplotlib' lib, added "Greys_r" to display the image in 'grey' form
def showimage(imageArray):
    plt.imshow(imageArray, cmap="Greys_r")
    plt.show()
    return